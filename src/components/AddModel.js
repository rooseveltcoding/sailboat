import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class AddModel extends Component {
	constructor() {
    super();
    this.onAdd = this.onAdd.bind(this);
  }

	getFields() {
		var fields = {};
		var attribs = this.props.attributes;
		for (var property in attribs) {
	    if (!attribs.hasOwnProperty(property)
					|| attribs[property].type === 'hidden'
					|| attribs[property].type === 'readonly') {
				continue;
			}
			fields[property] = ReactDOM.findDOMNode(this.refs[property]).value;
		}
		return fields;
	}

	clearFields() {
		var fields = {};
		var attribs = this.props.attributes;
		for (var property in attribs) {
	    if (!attribs.hasOwnProperty(property)
					|| attribs[property].type === 'hidden'
					|| attribs[property].type === 'readonly') {
				continue;
			}
			ReactDOM.findDOMNode(this.refs[property]).value = '';
		}
	}

	onAdd() {
		this.props
				.onAdd(this.getFields())
				.then(store => this.clearFields());
	}

	renderInputCells(model) {
		var cells = [];
		var attribs = this.props.attributes;
		for (var property in attribs) {
		    if (!attribs.hasOwnProperty(property)) {
					continue;
				}
				var attr = attribs[property];
				var inputType = attr.type || 'text';
				if (attr.type === 'readonly') {
					cells.push(<td key={property}></td>);
					continue;
				}

				cells.push(
					<td key={property} style={{display: inputType == 'hidden' ? 'none' : ''}}>
						{inputType == 'select' ? this.renderSelect(model, property, attr.options) : this.renderInput(model, property, inputType)}
					</td>
				);
		}

		return cells;
	}

	renderInput(model, prop, inputType) {
		return (
			<input type={inputType}
						 className="form-control"
						 ref={prop}
						 />
	 );
	}

	renderSelect(model, prop, options) {
		return (
			<select className="form-control" ref={prop}>
					{this.renderOptions(options)}
			</select>
	 );
	}

	renderOptions(options) {
		return options.map(opt => (<option key={opt} value={opt}>{opt}</option>))
	}

	render() {
		return (
			<tr>
				{ this.renderInputCells() }
				<td>
				  <button onClick={this.onAdd} type="submit" className="btn btn-primary">Submit</button>
				</td>
			</tr>
		);
	}
}
