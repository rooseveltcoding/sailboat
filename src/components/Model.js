import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Model extends Component {
	constructor() {
    super();
    this.updateState = this.updateState.bind(this);
  }

	updateState(e) {
		this.props.model[e.target.name] = e.target.value;
	}

	renderInputCells(model) {
		var cells = [];
		var attribs = this.props.attributes;
		for (var property in attribs) {
		    if (!attribs.hasOwnProperty(property)) {
					continue;
		    }

				var attr = attribs[property];
				if (attr.type === 'readonly') {
					var value = model[property];
					if(attr.format) {
						value = attr.format(value);
						if(typeof value === 'object' && value !== null) {
							console.error("'format' function returned an object.")
						}
					}
					cells.push(<td key={property}>{value}</td>);
					continue;
				}

				var inputType = attr.type || 'text';

				cells.push(
					<td key={property} style={{display: inputType == 'hidden' ? 'none' : ''}}>
						{inputType == 'select' ? this.renderSelect(model, property, attr.options) : this.renderInput(model, property, inputType)}
					</td>
				);
		}

		return cells;
	}

	renderInput(model, prop, inputType) {
		return (
			<input type={inputType}
						 name={prop}
						 className="form-control"
						 defaultValue={model[prop]}
						 onBlur={this.props.onChange}
						 onChange={this.updateState}/>
	 );
	}

	renderSelect(model, prop, options) {
		return (
			<select name={prop}
						  className="form-control"
						  defaultValue={model[prop]}
						  onBlur={this.props.onChange}
						  onChange={this.updateState}>
					{this.renderOptions(options)}
			 </select>
	 );
	}

	renderOptions(options) {
		return options.map(opt => (<option key={opt} value={opt}>{opt}</option>))
	}

	render() {
		const model = this.props.model;
		return (
				<tr>
					{ this.renderInputCells(this.props.model) }
					<td>
						<a href="javascript://" onClick={this.props.onDelete}>
							×
						</a>
					</td>
				</tr>
		);
	}
}
