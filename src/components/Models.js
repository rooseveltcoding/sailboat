import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Model from './Model';
import AddModel from './AddModel';

export default class Models extends Component {
	render() {
		return (
			<div>
				{ this.renderTable() }
			</div>);
	}

	renderAttributeHeaders() {
		var headerCells = [];
		for (var property in this.props.attributes) {
			var attrib = this.props.attributes[property];
			var style = {display: attrib.type == 'hidden' ? 'none' : ''};
			headerCells.push(<th key={property} style={style}>{attrib.display || property}</th>)
		}
		return headerCells;
	}

	renderModelRow(model, changeHandler, deleteHandler) {
		return (
			<Model key={model.id} model={model} attributes={this.props.attributes} onChange={changeHandler} onDelete={deleteHandler}/>
		);
	}

	renderAddModelRow(onAdd) {
		return (
			<AddModel onAdd={onAdd} attributes={this.props.attributes} />
		)
	}

	renderTable() {
		return (<table className="table">
			<thead>
				<tr>
					{ this.renderAttributeHeaders() }
					<th></th>
				</tr>
			</thead>
			<tbody>
				{ this.props.models.map((model, index) => {
					let changeHandler = () => {
						this.props.onChange(model);
					};
					let deleteHandler = () => {
						this.props.onDelete(model.id);
					};
					return this.renderModelRow(model, changeHandler, deleteHandler);
				}) }
			</tbody>
			<tfoot>
				{ this.renderAddModelRow(this.props.onAdd) }
			</tfoot>
		</table>);
	}
}
