import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import ghoulie from 'ghoulie';
import ReducerFactory from './ReducerFactory';

export default class StoreFactory {
  create(models) {
    const modelReducers = new ReducerFactory().create(models);
    modelReducers['ghoulieReducer'] = ghoulie.reducer();
    const rootReducer = combineReducers(modelReducers);
    const store = createStore(
    	rootReducer,
    	applyMiddleware(thunk)
    );
    return store;
  }
}
