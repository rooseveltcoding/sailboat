import {createActions, defaultActions} from '../utils/redux-helpers.js';

export default class ActionFactory {
  create(models, store) {
	const fetchOptions = {
		credentials: 'include',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		cache: 'default',
		mode: 'same-origin'  // if using CORS use mode: 'cors'
	};

	let actions = {};
	models.forEach((model) => {
		actions[model.pluralName] = createActions(() => store, fetchOptions, defaultActions(model.singularName, model.pluralName));
	});
	return actions;
  }
}
