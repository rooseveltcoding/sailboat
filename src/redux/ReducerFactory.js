import {createReducer, defaultReducers} from '../utils/redux-helpers.js';

export default class ReducerFactory {
	getInitialState() {
		return {
			error: null,
			loading: false,
			models: []
		}
	}

	create(models) {
		let reducers = {};
		models.forEach((model) => {
			var defaults = defaultReducers(model.singularName, model.pluralName);
			var reducer = createReducer(this.getInitialState, defaults);
			reducers[model.pluralName + 'Model'] = reducer;
		});
		return reducers;
	}
}
