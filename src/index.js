import React from 'react';
import {Route} from 'react-router';
import ReactDOM from 'react-dom';
import ModelsPage from './routes/ModelsPage'
import StoreFactory from './redux/StoreFactory'
import ActionFactory from './redux/ActionFactory'
import es6promise from 'es6-promise';
es6promise.polyfill();

export default class Sailboat {
	renderRoutes(models) {
		let routes = [];
		let store = new StoreFactory().create(models);
		let actions = new ActionFactory().create(models);
		models.forEach((model) => {
			routes.push(<Route key={model.pluralName} path={'/'+model.pluralName} component={() => {
				return (
					<ModelsPage model={model.pluralName} attributes={model.attributes} store={store} actions={actions}/>
				);
			}} />);
		});
		return routes;
	}
}
