import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Layout from './Layout';
import Models from '../components/Models';

export default class ModelsPage extends Component {
	constructor(props) {
		super(props);
		let pluralModelName = props.model;
		this.title = pluralModelName;
		this.logTitle = pluralModelName;
		this.model = this.title.toLowerCase();
		this.modelStateKey = this.model + 'Model';
		this.state = {
			models: this.getModels()
		};

		// when the store changes re-map the model to state
		this.props.store.subscribe(() => {
			this.setState({
				models: this.getModels()
			}, () => {

			});
		});
	}

	componentWillMount() {
		this.index();
	}

	index() {
		this.props.actions[this.model].index()
		.then(store => {
			this.setState({
				models: store[this.modelStateKey].models
			}, () => {
				const models = store[this.modelStateKey].models;
			});
		})
		.catch(function(e, store) {
			console.log('CAUGHT ERROR', e);
		});
	}

	render() {
		var id = `page-${this.model}`;
		return (
			<Layout title={this.title}>
				<div id={id} className="page">
					{ this.renderErrors() }
					{ this.renderComponent(this.state.models, this.onAdd, this.onChange, this.onDelete) }
				</div>
			</Layout>);
	}

	// renderComponent() {
	// 	throw "renderComponent not implemented!";
	// }
	renderComponent(models, onAdd, onChange, onDelete) {
		return (
			<Models attributes={this.props.attributes} models={models} onAdd={onAdd} onChange={onChange} onDelete={onDelete} />
		);
	}

	renderErrors() {
		const style = {
			height: '75px',
			visibility: this.state.error ? 'visible' : 'hidden'
		};
		return (
			<div className="alert alert-danger" style={style} role="alert">{this.state.error}</div>
		);
	}

	getModels() {
		return this.props.store.getState()[this.modelStateKey].models;
	}

	onAdd(attributes) {
		return this.props.actions[this.model].create(attributes)
		.then(this.index)
		.catch(error => {
			console.log(error);
		});
	}

	onChange(attributes) {
		return this.props.actions[this.model].update(attributes, {
			id: attributes.id,
		})
		.then(this.index)
		.catch(error => {
			console.log(error);
		});
	}

	onDelete(id) {
		if (confirm("Are you sure you want to delete this?")) {
			this.props.actions[this.model].delete(null, {
				id
			}).then(this.index);
		}
	}
}
