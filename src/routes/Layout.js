import React, {Component} from 'react';

export default class Layout extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	// static propTypes = {
	// 	title: React.PropTypes.string.isRequired
	// };

	render() {
		return (
			<div id="layout">
				{this.props.children}
			</div>
		);
	}
}
